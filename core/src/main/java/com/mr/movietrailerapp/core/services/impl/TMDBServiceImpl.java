package com.mr.movietrailerapp.core.services.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mr.movietrailerapp.core.services.TMDBConfiguration;
import com.mr.movietrailerapp.core.services.TMDBService;
import com.mr.movietrailerapp.core.utils.Network;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import static com.mr.movietrailerapp.core.utils.CommonUtils.get;

/**
 * @author Mahesh MR
 * <p>
 * Implementation class of YoutubeServiceImpl interface and this class reads values from the OSGi configuration as well
 */
@Component(service = TMDBService.class, immediate = true)
@Designate(ocd = TMDBConfiguration.class)
public class TMDBServiceImpl implements TMDBService {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(TMDBServiceImpl.class);

    /**
     * Instance of the OSGi configuration class
     */
    private TMDBConfiguration configuration;

    @Activate
    protected void activate(TMDBConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Overridden method of the YoutubeService
     */
    @Override
    public String searchMovies(SlingHttpServletRequest request) {
        try {
            String key = configuration.getKey();
            String baseUrl = configuration.getBaseURL();
            String additionalPaths = get(request, "additionalPaths") == null ? StringUtils.EMPTY : get(request, "additionalPaths");
            additionalPaths = replaceInputs(additionalPaths, "tempkeyreplace", key);
            additionalPaths = replaceInputs(additionalPaths, "tempBaseURL", baseUrl);

            String requestURL = additionalPaths;

            LoadingCache<String, String> resultsCache =
                    CacheBuilder.newBuilder()
                            .maximumSize(10000)                             // maximum 10000 records can be cached
                            .expireAfterAccess(30, TimeUnit.MINUTES)      // cache will expire after 30 minutes of access
                            .build(new CacheLoader<String, String>() {  // build the cacheloader
                                @Override
                                public String load(String url) {
                                    return Network.readJson(url);
                                }
                            });

            return resultsCache.get(requestURL);

        } catch (Exception e) {
            log.error("Exception in TMDB service request call", e);
        }
        return null;
    }

    private String replaceInputs(String additionalPaths, String oldValue, String replacementValue) {
        if (additionalPaths != null) {
            additionalPaths = additionalPaths.replace(oldValue, replacementValue);
        }
        return additionalPaths;
    }
}
