package com.mr.movietrailerapp.core.utils;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtils {

    private CommonUtils() {}

    private static final Logger log = LoggerFactory.getLogger(CommonUtils.class);

    public static String get(SlingHttpServletRequest request, String parameterName) {
        String value = null;
        try {
            value = request.getParameter(parameterName);
        } catch (Exception e) {
            log.error("Exception when retrieving the parameter details : {}", parameterName);
        }
        return value;
    }
}
