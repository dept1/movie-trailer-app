package com.mr.movietrailerapp.core.services;


import org.apache.sling.api.SlingHttpServletRequest;

/**
 * @author Mahesh MR
 * <p>
 * This interface exposes the functionality of calling a JSON Web Service
 */
public interface YoutubeService {

    /**
     * This method makes the HTTP call on the given UR
     *
     * @return {@link String}
     */
    String getResults(SlingHttpServletRequest request) throws Exception;
}
