package com.mr.movietrailerapp.core.services;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Mahesh MR
 * <p>
 * This interface represents an OSGi configuration which can be found at -
 * ./system/console/configMgr
 */
@ObjectClassDefinition(
        name = "Youtube Configuration",
        description = "This configuration reads the values to make call to the youtube api v3 service")
public @interface YoutubeAPIConfiguration {

    /**
     * Returns the endpoint
     *
     * @return {@link String}
     */
    @AttributeDefinition(
            name = "endpoint",
            description = "Enter full Server URL with endpoint details")
    String getEndpoint();

    /**
     * Returns the key
     *
     * @return {@link String}
     */
    @AttributeDefinition(
            name = "key",
            description = "Enter the Youtube Data API v3 key")
    String getKey();
}