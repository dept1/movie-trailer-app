package com.mr.movietrailerapp.core.servlets;

import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_PATHS;
import static com.mr.movietrailerapp.core.utils.CommonUtils.get;

@Component(
        service = {Servlet.class},
        property = {
                SLING_SERVLET_PATHS + "=/movieapp/contact"
        }
)
public class SendEmailServlet extends SlingAllMethodsServlet {

    private static Logger log = LoggerFactory.getLogger(SendEmailServlet.class);

    @Reference
    private transient MessageGatewayService messageGatewayService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        JSONObject jsonResponse = new JSONObject();
        boolean sent = false;
        try {
            String name = get(request, "name");
            String email = get(request, "email");
            String subject = get(request, "subject");
            String message = get(request, "message");
            String receiverEmail = get(request, "receiverEmail");
            String[] recipients = {receiverEmail};
            final String TD_START = "\t\t\t<td>";
            final String TR_END = "\t\t</tr>\n";
            final String TD_END = "</td>\n";
            final String TR_START = "\t\t<tr>\n";

            String finalMessage = "<h1>Hi There!</h1>\n"
                    + "<p style='color:green;'>There is a new submission on the website!\n\n\n</p>"
                    + "<table style=\"border:black solid 1px;text-align: center;padding: 2px;margin: 1px;color: teal;\">\n" +
                    "\t<tbody>\n" +
                    TR_START +
                    "\t\t\t<td>Name</td>\n" +
                    TD_START+name+TD_END +
                    TR_END +
                    TR_START +
                    "\t\t\t<td>Email</td>\n" +
                    TD_START+email+TD_END +
                    TR_END +
                    TR_START +
                    "\t\t\t<td>Subject</td>\n" +
                    TD_START+subject+TD_END +
                    TR_END +
                    TR_START +
                    "\t\t\t<td>Message</td>\n" +
                    TD_START+message+TD_END +
                    TR_END +
                    "\t</tbody>\n" +
                    "</table>";
            sendEmail("New Request from site",
                    finalMessage, recipients);
            response.setStatus(200);
            sent = true;
        } catch (EmailException e) {
            response.setStatus(500);
        }
        try {
            jsonResponse.put("result", sent ? "done" : "something went wrong");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        response.getWriter().write(jsonResponse.toString());
    }

    private void sendEmail(String subjectLine, String msgBody, String[] recipients) throws EmailException {
        Email email = new HtmlEmail();
        for (String recipient : recipients) {
            email.addTo(recipient, recipient);
        }
        email.setSubject(subjectLine);
        email.setMsg(msgBody);
        MessageGateway<Email> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
        if (messageGateway != null) {
            log.debug("sending out email");
            messageGateway.send(email);
        } else {
            log.error("The message gateway could not be retrieved.");
        }
    }
}