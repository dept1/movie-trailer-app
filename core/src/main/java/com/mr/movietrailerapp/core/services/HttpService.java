package com.mr.movietrailerapp.core.services;

/**
 * @author Mahesh MR
 *
 * This interface exposes the functionality of calling a JSON Web Service
 */
public interface HttpService {

    /**
     * This method makes the HTTP call on the given URL
     * @return {@link String}
     */
    public String makeHttpCall();
}