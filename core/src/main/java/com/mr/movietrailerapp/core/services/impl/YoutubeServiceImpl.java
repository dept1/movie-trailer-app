package com.mr.movietrailerapp.core.services.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mr.movietrailerapp.core.services.YoutubeAPIConfiguration;
import com.mr.movietrailerapp.core.services.YoutubeService;
import com.mr.movietrailerapp.core.utils.Network;
import org.apache.sling.api.SlingHttpServletRequest;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

import static com.mr.movietrailerapp.core.utils.CommonUtils.get;

/**
 * @author Mahesh MR
 * <p>
 * Implementation class of YoutubeServiceImpl interface and this class reads values from the OSGi configuration as well
 */
@Component(service = YoutubeService.class, immediate = true)
@Designate(ocd = YoutubeAPIConfiguration.class)
public class YoutubeServiceImpl implements YoutubeService {

    /**
     * Logger
     */
    private static final Logger log = LoggerFactory.getLogger(YoutubeServiceImpl.class);

    /**
     * Instance of the OSGi configuration class
     */
    private YoutubeAPIConfiguration configuration;

    @Activate
    protected void activate(YoutubeAPIConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Overridden method of the YoutubeService
     */
    @Override
    public String getResults(SlingHttpServletRequest request) {
        try {
            String key = configuration.getKey();
            String endpoint = configuration.getEndpoint();
            String q = get(request, "q");
            String maxResults = get(request, "maxResults");
            String pageToken = get(request, "pageToken");
            String order = get(request, "order");
            String videoDuration = get(request, "videoDuration");

            StringBuilder finalURL = new StringBuilder();

            finalURL.append(endpoint).append("?key=").append(key).append("&part=snippet&q=").append(q)
                    .append("&maxResults=").append(maxResults)
                    .append("&order=").append(order).append("&videoDuration=")
                    .append(videoDuration).append("&type=video");
            if (pageToken != null) {
                finalURL = finalURL.append("&pageToken=").append(pageToken);
            }

            LoadingCache<String, String> resultsCache =
                    CacheBuilder.newBuilder()
                            .maximumSize(10000)                             // maximum 10000 records can be cached
                            .expireAfterAccess(30, TimeUnit.MINUTES)      // cache will expire after 30 minutes of access
                            .build(new CacheLoader<String, String>() {  // build the cacheloader
                                @Override
                                public String load(String url) {
                                    return Network.readJson(url);
                                }
                            });

           return resultsCache.get(finalURL.toString());

        } catch (Exception e) {
            log.error("Exception in youtube service call", e);
        }
        return null;
    }
}
