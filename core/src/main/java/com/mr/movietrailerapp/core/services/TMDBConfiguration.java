package com.mr.movietrailerapp.core.services;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Mahesh MR
 * <p>
 * This interface represents an OSGi configuration which can be found at -
 * ./system/console/configMgr
 */
@ObjectClassDefinition(
        name = "TMDB Configuration",
        description = "This configuration reads the values to make call to the TMDB api v3 service")
public @interface TMDBConfiguration {

    /**
     * Returns the key
     *
     * @return {@link String}
     */
    @AttributeDefinition(
            name = "key",
            description = "Enter the TMDB Data API v3 key")
    String getKey();

    /**
     * Returns the Base URL
     *
     * @return {@link String}
     */
    @AttributeDefinition(
            name = "BASE URL",
            description = "Enter Base URL")
    String getBaseURL();
}