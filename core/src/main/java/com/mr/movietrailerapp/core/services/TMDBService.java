package com.mr.movietrailerapp.core.services;


import org.apache.sling.api.SlingHttpServletRequest;

/**
 * @author Mahesh MR
 * <p>
 * This interface exposes the functionality of calling a JSON Web Service
 */
public interface TMDBService {

    /**
     * This method makes the HTTP call on the given UR
     *
     * @return {@link String}
     */
    String searchMovies(SlingHttpServletRequest request) throws Exception;
}
