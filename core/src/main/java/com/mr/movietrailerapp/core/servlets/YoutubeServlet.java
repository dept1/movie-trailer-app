package com.mr.movietrailerapp.core.servlets;

import com.mr.movietrailerapp.core.services.YoutubeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

import static org.apache.sling.api.servlets.ServletResolverConstants.SLING_SERVLET_PATHS;

@Component(
        service = {Servlet.class},
        property = {
                SLING_SERVLET_PATHS + "=/movieapp/youtubeSearch"
        }
)
public class YoutubeServlet extends SlingAllMethodsServlet {

    private static Logger log = LoggerFactory.getLogger(YoutubeServlet.class);

    @Reference
    private transient YoutubeService youtubeService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        this.doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        String resultJson = StringUtils.EMPTY;
        try {
            resultJson = youtubeService.getResults(request);
        } catch (Exception e) {
            response.setStatus(500);
            log.error("Exception occurred in YoutubeServlet:doPost()", e);
        }
        response.getWriter().println(resultJson);
    }
}