$(document).ready(function () {
  var url = '/movieapp/youtubeSearch';
  var $pagination = $("#pagination"),
    totalRecords = 0,
    records = [],
    recPerPage = 0,
    nextPageToken = "",
    totalPages = 0;
  var search = "";
  var duration = "any";
  var order = "relevance";

  var maxResults=10
  $("#duration").change(function () {
    duration = $(this).children("option:selected").val();
  });
  $("#order").change(function () {
    order = $(this).children("option:selected").val();
  });

  $("#myForm").submit(function (e) {
    e.preventDefault();

    search = $("#search").val();


    $.ajax({
      method: "GET",
      url: url,
      data : {
        videoDuration: duration,
        order: order,
        maxResults: maxResults,
        q: search
      },
      beforeSend: function () {
        $("#btn").attr("disabled", true);
        $("#results").empty();
      },
      success: function (data) {
        $("#btn").attr("disabled", false);
        var jsonData = JSON.parse(data);
        displayVideos(jsonData);
      },
    });
  });

  function apply_pagination() {
    $pagination.twbsPagination({
      totalPages: totalPages,
      visiblePages: 6,
      onPageClick: function (event, page) {
        displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
        endRec = displayRecordsIndex + recPerPage;
        displayRecords = records.slice(displayRecordsIndex, endRec);
        generateRecords(recPerPage, nextPageToken);
      },
    });
  }

  $("#search").change(function () {
    search = $("#search").val();
  });

  function generateRecords(recPerPage, nextPageToken) {
    $.ajax({
      method: "GET",
      url: url,
      data : {
        videoDuration: duration,
        order: order,
        maxResults: maxResults,
        q: search,
        pageToken: nextPageToken
      },
      beforeSend: function () {
        $("#btn").attr("disabled", true);
        $("#results").empty();
      },
      success: function (data) {
        $("#btn").attr("disabled", false);
        var jsonData = JSON.parse(data);
        displayVideos(jsonData);
      },
    });
  }

  function displayVideos(data) {
    recPerPage = data.pageInfo.resultsPerPage;
    nextPageToken = data.nextPageToken;
    totalRecords = data.pageInfo.totalResults;
    totalPages = Math.ceil(totalRecords / recPerPage);
    apply_pagination();
    $("#search").val("");

    var videoData = "";

    $("#table").show();

    data.items.forEach((item) => {
      videoData = `

                    <tr>
                    <td>
                    ${item.snippet.title}<br/>
                    <a target="_blank" href="https://www.youtube.com/channel/${item.snippet.channelId}">${item.snippet.channelTitle}</a>
                    <lite-youtube videoid="${item.id.videoId}" style="background-image: url('https://i.ytimg.com/vi/${item.id.videoId}/hqdefault.jpg');">
                      <a href="https://youtube.com/watch?v=${item.id.videoId}" class="lty-playbtn" title="${item.snippet.title}">
                        <span class="lyt-visually-hidden">Play Video: ${item.snippet.title}</span>
                      </a>
                    </lite-youtube>
                    </td>
                    </tr>

                    `;

      $("#results").append(videoData);
    });
  }
});
