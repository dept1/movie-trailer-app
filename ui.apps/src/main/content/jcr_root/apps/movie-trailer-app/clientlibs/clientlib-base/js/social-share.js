(function($) {
	"use strict";
    const links = $("[data-share-type]");
    links.click(function(e){
        let type = $(this).attr('data-share-type');
        let href = $(this).attr('href');
        let text = $(this).attr('data-share-title');
        let currentPageUrl = window.location.href;
        let finalUrl = '';
        switch (type) {
            case 'facebook':
                    let facebookUrl = "u=" + currentPageUrl;
                    finalUrl = href + facebookUrl;
                    break;
            case 'linkedin':
                    let linkedInUrl = "mini=true&source=LinkedIn.com&title=" + text + '&url= ' + currentPageUrl;
                    finalUrl = href + linkedInUrl
                    break;
            case 'twitter':
                    let twitterUrl = "url=" + currentPageUrl + "&text=" + text;
                    finalUrl = href + twitterUrl;
                    break;
            case 'whatsapp':
                    let whatsappUrl = "text=" + currentPageUrl;
                    finalUrl = href + whatsappUrl;
        }
        e.preventDefault();
        window.open(finalUrl,"","width=500,height=500");
    });
})(jQuery);
